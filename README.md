## Setup

Create `local_config.js` file with app id, your page id and access token:

```
module.exports = {
  app_id: '558344027679...',
  page_id: '139516193134...',
  page_access_token: '...'
};
```

Install modules and start app

```
npm install
npm start
```
