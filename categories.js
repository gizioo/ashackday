var categories = ['id','name','about','location','birthday','devices', 
                  'hometown','relationship_status','likes{name,category,id}','books{category,name,id}',
                  'movies{category,name,id}','music','family','feed.limit(10){with_tags,place,created_time}'];
                  
module.exports =  categories;