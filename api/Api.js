var when = require('when');
var knex = require('knex');
var db_config = require('../db_config');
var ranking = require('../bot/ranking');

/**
 * @constructor
 */
var Api = function() {
  this.handler = knex({
    client: 'pg',
    connection: {
      host     : db_config.db_host,
      user     : db_config.db_user,
      password : db_config.db_password,
      database : db_config.db_name
    }
  });
};

Api.prototype.getGenreBooks = function(genre, limit) {
  return this.handler.select().from('books')
    .where({
      book_genre: genre.toLowerCase()
    })
    .limit(limit)
    .orderBy('book_year', 'desc')
};

Api.prototype.getGenreBooksThree = function(genre, limit) {
  return this.handler.distinct('book_genre', 'book_title', 'book_author', 'book_img_url').from('books')
    .where({
      book_genre: genre[0].toLowerCase()
    })
    .orWhere('book_genre', genre[1])
    .orWhere('book_genre', genre[2])
    .select()
    .limit(limit)
};

Api.prototype.getAuthorBooks = function(author, limit) {
  
  var authorSecUpper = author.indexOf('-')+1;
  var authorReplaced = author.replace(author[authorSecUpper], 
    author[authorSecUpper].toUpperCase()).replace('-',' ');

  return this.handler.select().from('books')
    .where(
      'book_author','like', '%'+capitalizeFirstLetter(authorReplaced)+'%'
    )
    .limit(limit);
};

Api.prototype.getBook = function(title, limit, callback) {
  
  var titleSplitted = title.split('-').join('%');
  
  return this.handler.select().from('books')
    .where(
      'book_title','like', '%'+capitalizeFirstLetter(titleSplitted)+'%'
    )
    .limit(limit);
};

Api.prototype.saveUserP = function(user_fb_id, user_msg_id, user_access_token, add_info) {
  var that = this;
    return this.handler.select('user_id').from('users').where({user_fb_id: user_fb_id})
      .then(function(user) {
          if(Object.keys(user).length === 0){
            var calculated_rank = {};

              return ranking.calcRank(add_info).then(function(calculated_rank){
                return that.handler.insert({
                    user_fb_id: user_fb_id, 
                    user_msg_id: user_msg_id,
                    user_access_token: user_access_token,
                    user_info: add_info,
                    user_rank: calculated_rank
                }, 'user_info').into('users')
              });
          }else{
            return new Error('User exists.');
          }
      })
};

/**
 * @param {string} user_messenger_id
 * @returns {Promise.<Object>} user record
 */
Api.prototype.getUser = function(user_messenger_id) {
  return this.handler
    .first()
    .from('users')
    .where({
      user_msg_id: user_messenger_id
    });
};


Api.prototype.updateState = function(user_messenger_id, new_state) {
  return this.handler('users')
    .where('user_msg_id', '=', user_messenger_id)
    .update({
      state: new_state
    });
};

/**
 * @param {string} user_messenger_id
 * @param {string[]} new_order list of scenario labels (people, interest,
 *   location), not yet "done" by user, sorted by `sum` parameter
 */
Api.prototype.updateRankedScenarios = function(user_messenger_id, new_order) {
  return this.handler('users')
    .where('user_msg_id', '=', user_messenger_id)
    .update({
      ranked_scenarios: JSON.stringify(new_order)
    });
};

/**
 * @param {string} genre1
 * @param {string} genre2
 * @param {string} genre3
 * Prepared data sets:
  'culinary','travel','history'
  'art','germany','football'
  'horror','comics','family'
  * @returns {Promise.Object[3]} user record
 */
Api.prototype.getThreeCommonBooks = function(genre1, genre2, genre3) {
  return this.handler.select('*')
    .from('books')
    .whereIn('book_genre', [genre1, genre2, genre3])
    .limit(3)
    .orderByRaw('book_title ASC');
};

Api.prototype.fullTextSearch = function(search_string){
  var that = this;
  var searchString = search_string.split(" ").join(" | ");
  return when.resolve("select b_title, b_author, b_genre, b_img from (select books.book_title as b_title, books.book_img_url as b_img,  books.book_author as b_author, books.book_genre as b_genre, to_tsvector(books.book_author) || to_tsvector(books.book_title) || to_tsvector(books.book_genre) as document from books) p_search WHERE p_search.document @@ to_tsquery('"+searchString+"') ORDER BY ts_rank(p_search.document, to_tsquery('"+searchString+"')) DESC").then(function(searchQuery){
        return that.handler.raw(searchQuery);
  });
}

Api.prototype.fullTextSearchOrderByYear = function(search_string){
  var that = this;
  var searchString = search_string.split(" ").join(" | ");
  return when.resolve("select b_title, b_author, b_genre, b_img from (select books.book_title as b_title, books.book_year as b_year, books.book_img_url as b_img,  books.book_author as b_author, books.book_genre as b_genre, to_tsvector(books.book_author) || to_tsvector(books.book_title) || to_tsvector(books.book_genre) as document from books) p_search WHERE p_search.document @@ to_tsquery('"+searchString+"') ORDER BY b_year DESC LIMIT 3").then(function(searchQuery){
        return that.handler.raw(searchQuery);
  });
}
/**
 * @param {string} word
 * @returns {First letter upperCase string}
 */
function capitalizeFirstLetter(string) {
    return string[0].toUpperCase() + string.slice(1);
}

module.exports = Api;

