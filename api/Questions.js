var when = require('when');
var Api = require('./Api');
var ranking = require('../bot/ranking');

var api = new Api();

/**
 * @param {string} user_messenger_id
 * @returns {Promise.Object[]}
 *
 [ { name: 'Maja Rzazewska',
    id: '289702274751209',
    relationship: 'daughter' } ]
 *
 */
function getUserFamily(user_messenger_id) {
  return when.promise(function(resolve, reject) {
    api.getUser(user_messenger_id).then(function(body) {
      if (!body) {
        return reject('No user data!');
      }
      if(body.user_info.hasOwnProperty('family')){
        return resolve(body.user_info.family.data);
      }
      else{
        return resolve([]);
      }
    });
  });
}

/**
 * @param {string} user_messenger_id
 * @returns {Promise.Object}
 *
 { person: { 
      name: 'Katarzyna Sielicka',
      info: 'For sure not a family member' 
    } 
 }
 *
 */
function getUserCommonFriend(user_messenger_id) {
  return when.promise(function(resolve, reject) {
    api.getUser(user_messenger_id).then(function(body) {
      var friends_tagged = [];
      var occurence = 0;
      if (!body) {
        return reject('No user data!');
      }
      try { // temporary fix
        if(body.user_info.hasOwnProperty('feed')){
          (body.user_info.feed.data).forEach(function(elem) {
            if(elem.hasOwnProperty('with_tags'))
              friends_tagged.push(elem.with_tags.data[0].name);
          });
          var common_friend = ranking.getMaxOccurrence(friends_tagged);
          if(body.user_info.hasOwnProperty('family')){
            (body.user_info.family.data).forEach(function(elem) {
                if(elem.name == common_friend)
                  occurence += 1;
            });
          }
          if(occurence === 0){
            return resolve({
              'person':{
                'name' : common_friend,
                'info' : 'For sure not a family member'
              }
            });
          }
          else {
            return resolve({
              'person':{
                'name' : common_friend,
                'info' : 'May be a family member'
              }
            });
          }
        }
      } catch (error) {
        console.log(error);
        return resolve(null);
      }
    });
  });
}

/**
 * @param {string} user_messenger_id
 * @returns {Promise.Bool}
 *
 True/Fale
 *
 */
function isUserInRelationship(user_messenger_id) {
  return when.promise(function(resolve, reject) {
    api.getUser(user_messenger_id).then(function(body) {
      if (!body) {
        return reject('No user data!');
      }
      if(body.user_info.hasOwnProperty('relationship_status')){
        if(body.user_info.relationship_status === 'In a relationship' || 
           body.user_info.relationship_status === 'Married' || 
           body.user_info.relationship_status === 'Engaged')
          return resolve(true);
        else
          return resolve(false);
      }
      else
        return resolve(false);
    });
  });
}

/**
 * @param {string} user_messenger_id
 * @returns {Promise.Object[]}
 *
 [ { name: 'Bogusław Wołoszański',
     category: 'Book',
     id: '105683516137090' },
   { name: 'Where Eagles Dare',
     category: 'Movie',
     id: '1375221962723391' },
   { name: 'Pulp Fiction',
     category: 'Movie',
     id: '112842988801190' },
   { name: 'Bogusław Wołoszański',
     category: 'Author',
     id: '421541494622740' }
    ]
 *
 */
function getUserLikes(user_messenger_id) {
  return when.promise(function(resolve, reject) {
    api.getUser(user_messenger_id).then(function(body) {
      if (!body) {
        return reject('No user data!');
      }
      if(body.user_info.hasOwnProperty('likes')){
        var music_band = ranking.getCategory(body.user_info.likes.data, "Musician/Band");
        var book = ranking.getCategory(body.user_info.likes.data, "Book");
        var movie = ranking.getCategory(body.user_info.likes.data, "Movie");
        var author = ranking.getCategory(body.user_info.likes.data, "Author");
        var results = music_band.concat(book).concat(movie).concat(author);
        return resolve(results);
      }
    });
  });
}

/**
 * @param {string} user_messenger_id
 * @returns {Promise.Object[]}
 *
 [{
   "category": "Actor/Director",
   "name": "Michael Moore",
   "id": "105689932798411"
 },
 {
   "category": "Book",
   "name": "Bogusław Wołoszański",
   "id": "105683516137090"
}]
 *
 */
function getUserBooksSection(user_messenger_id) {
  return when.promise(function(resolve, reject) {
    api.getUser(user_messenger_id).then(function(body) {
      if (!body) {
        return reject('No user data!');
      }
      if (body.user_info.hasOwnProperty('books')) {
        return resolve(body.user_info.books.data);
      }
    });
  });
}

/**
 * @param {string} user_messenger_id
 * @returns {Promise.Object[]}
 *
 [{
   "category": "Music",
   "name": "Apocalyptica",
   "id": "165017321138"
 },
 {
   "category": "Movie",
   "name": "Where Eagles Dare",
   "id": "1375221962723391"
 },
 {
   "category": "Movie",
   "name": "4 Months, 3 Weeks and 2 Days",
   "id": "107325019296740"
 }]
 *
 */
function getUserOtherInterests(user_messenger_id) {
  return when.promise(function(resolve, reject) {
    api.getUser(user_messenger_id).then(function(body) {
      if (!body) {
        return reject('No user data!');
      }
      if(body.user_info.hasOwnProperty('movies') || body.user_info.hasOwnProperty('music')){
        var results = [];
        var movies = [];
        var music = [];
        if (body.user_info.movies && body.user_info.movies.data){
          movies = body.user_info.movies.data;
        }
        if (body.user_info.music && body.user_info.music.data){
          music = body.user_info.music.data;
        }
        results = movies.concat(music);
        return resolve(results);
      }
    });
  });
}


/**
 * @param {string} user_messenger_id
 * @returns {Promise.Array}
 *
 ['Warszawa']
 *
 */
function getUserCurrentLocation(user_messenger_id) {
  return when.promise(function(resolve, reject) {
    api.getUser(user_messenger_id).then(function(body) {
      if (!body) {
        return reject('No user data!');
      }
      var cities = [];
      var home_location = [];
      var results = [];
      if(body.user_info.hasOwnProperty('feed')){
        (body.user_info.feed.data).forEach(function(elem) {
          if(elem.hasOwnProperty('place') && elem.hasOwnProperty('created_time') ){
            if(elem.place.hasOwnProperty('location')){
                var difference = new Date() - new Date(elem.created_time);
                if(ranking.milisecondToDays(difference)<= 0){
                  cities.push(elem.place.location.city);
                }
            }
          }
        });
        if(body.user_info.hasOwnProperty('location') || body.user_info.hasOwnProperty('hometown')){
          if(body.user_info.hasOwnProperty('location'))
            home_location.push(body.user_info.location.name);
          if(body.user_info.hasOwnProperty('hometown'))
            home_location.push(body.user_info.hometown.name);
          var splitted_home_locations = home_location.join(',').split(',');
          cities.forEach(function(elem) {
            if(splitted_home_locations.indexOf(elem)==-1)
              results.push(elem);
          });
        }
      }
      return resolve(results);  
    });
  });
}

/**
 * @param {string} user_messenger_id
 * @returns {Promise.String}
 *
'Amsterdam'
 *
 */
function getUserFavoritePlace(user_messenger_id) {
  return when.promise(function(resolve, reject) {
    api.getUser(user_messenger_id).then(function(body) {
      if (!body) {
        return reject('No user data!');
      }
      var all_visited_location = [];
      var common_city_occurence = 0;
      var splitted_home_locations = [];
      if(body.user_info.hasOwnProperty('feed')){
        (body.user_info.feed.data).forEach(function(elem) {
          if(elem.hasOwnProperty('place')){
            if(elem.place.hasOwnProperty('location')){
                all_visited_location.push(elem.place.location.city);
            }
          }
        });
        var common_city = ranking.getMaxOccurrence(all_visited_location);
        all_visited_location.forEach(function(elem) {
          if (elem == common_city) {
            common_city_occurence += 1;
          }
        });
        if (splitted_home_locations.indexOf(common_city)==-1) {
          if (common_city_occurence/all_visited_location.length > 0.1) {
            return resolve(common_city);
          }
        }
      }
    });
  });
}

/**
 * @param {string} user_messenger_id
 * @returns {Promise.Array}
 *
['Amsterdam','Warszawa','Radom']
 *
 */
function getUserVisitedLocations(user_messenger_id) {
  return when.promise(function(resolve, reject) {
    api.getUser(user_messenger_id).then(function(body) {
      if (!body) {
        return reject('No user data!');
      }
      var all_visited_location = [];
      var common_city_occurence = 0;
      var splitted_home_locations = [];
      if(body.user_info.hasOwnProperty('feed')){
        (body.user_info.feed.data).forEach(function(elem) {
          if(elem.hasOwnProperty('place')){
            if(elem.place.hasOwnProperty('location')){
                all_visited_location.push(elem.place.location.city);
            }
          }
        });
      }
      var result = all_visited_location.filter (function(v, i, a) { 
        return a.indexOf (v) == i 
      });
      if (result.length>=3) {
        return resolve(result);
      }else{
        return resolve([]);
      }
    });
  });
}

module.exports = {
  getUserFamily: getUserFamily, //People q1
  getUserCommonFriend: getUserCommonFriend, //People q2
  isUserInRelationship: isUserInRelationship, //People q3
  getUserLikes: getUserLikes, //Interest q1
  getUserBooksSection: getUserBooksSection, //Interest q2
  getUserOtherInterests: getUserOtherInterests, //Interest q3
  getUserCurrentLocation: getUserCurrentLocation, //Location q1
  getUserFavoritePlace: getUserFavoritePlace, //Location q2
  getUserVisitedLocations: getUserVisitedLocations //Location q3
};
