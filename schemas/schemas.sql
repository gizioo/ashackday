CREATE TABLE users
(
  user_id uuid NOT NULL DEFAULT uuid_generate_v4(),
  user_fb_id text,
  user_msg_id text,
  user_access_token text,
  user_info json,
  state json,
  user_rank json,
  ranked_scenarios json
)

CREATE TABLE books
(
  book_id uuid NOT NULL DEFAULT uuid_generate_v4(),
  book_title text,
  book_author text,
  book_genre text,
  book_img_url text,
  book_url text,
  book_year integer
)

INSERT INTO books(book_title, book_author, book_genre, book_img_url, book_url, book_year) VALUES ( 'Dave Barrys Only Travel Guide Youll Ever Need', 'Dave Barry', 'guidebook', 'http://d.gr-assets.com/books/1320433298l/126026.jpg', '',2016);
INSERT INTO books(book_title, book_author, book_genre, book_img_url, book_url, book_year) VALUES ( 'A Walk In The Woods: The Worlds Funniest Travel Writer Takes a Hike', 'Bill Bryson', 'guidebook', 'http://d.gr-assets.com/books/1443896531l/24909835.jpg', '',2016);
INSERT INTO books(book_title, book_author, book_genre, book_img_url, book_url, book_year) VALUES ( 'Amsterdam (Insight Guides - Amsterdam)', 'Deni Brown', 'guidebook', 'http://d.gr-assets.com/books/1387734106l/738468.jpg', '',2016);
INSERT INTO books(book_title, book_author, book_genre, book_img_url, book_url, book_year) VALUES ( 'Where To Go When (Eyewitness Travel Guides)', 'Joseph Rosendo', 'guidebook', 'http://d.gr-assets.com/books/1347401990l/1916181.jpg', '',2016);
INSERT INTO books(book_title, book_author, book_genre, book_img_url, book_url, book_year) VALUES ( 'Berlin - a Choose a Way interactive', 'Sam Wood', 'travel', 'http://d.gr-assets.com/books/1455287552l/29078593.jpg', '',2016);
INSERT INTO books(book_title, book_author, book_genre, book_img_url, book_url, book_year) VALUES ( 'Divided Berlin, 1945-1990: The Historical Guidebook', 'Oliver Boyn', 'travel', 'http://d.gr-assets.com/books/1358739542l/14635797.jpg', '',2016);
INSERT INTO books(book_title, book_author, book_genre, book_img_url, book_url, book_year) VALUES ( 'Berlin for Free; A Guidebook to Movies, Music, Museums, and Many More Free and Cheap Sightseeing Destinations for the Frugal Traveler, Updated Edition','Traveler', 'travel', 'http://d.gr-assets.com/books/1347724793l/9937906.jpg', '',2016);
INSERT INTO books(book_title, book_author, book_genre, book_img_url, book_url, book_year) VALUES ( 'Rock Music Styles: a history ', 'Katherine Charlton', 'music', 'http://d.gr-assets.com/books/1347588968l/641427.jpg', '',2016);
INSERT INTO books(book_title, book_author, book_genre, book_img_url, book_url, book_year) VALUES ( 'A History of Rock Music: 1951-2000', 'Piero Scaruffi', 'music', 'http://d.gr-assets.com/books/1173942249l/347967.jpg', '',2016);
INSERT INTO books(book_title, book_author, book_genre, book_img_url, book_url, book_year) VALUES ( 'The Music And Art Of Radiohead (Ashgate Popular & Folk Music)', 'Joseph Tate', 'music', 'http://d.gr-assets.com/books/1391583634l/388282.jpg', '',2016);
INSERT INTO books(book_title, book_author, book_genre, book_img_url, book_url, book_year) VALUES ( 'Encyclopedia of Punk Music and Culture', 'Brian Cogan', 'music', 'http://d.gr-assets.com/books/1347982802l/235099.jpg', '',2016);


