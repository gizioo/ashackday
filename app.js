var express = require('express');
var path = require('path');
var favicon = require('serve-favicon');
var logger = require('morgan');
var cookieParser = require('cookie-parser');
var bodyParser = require('body-parser');
var hbs = require('express-handlebars');
var local_config = require('./local_config');
var db_config = require('./db_config');

var routes = require('./routes/index');
var bot_routes = require('./routes/bot');
var learn = require('./routes/learn');
var api_routes = require('./routes/api');

var app = express();

if (!local_config.app_id || !local_config.page_id || !local_config.page_access_token) {
  console.warn('Please configure `local_config.js`. See README.md');
}

if (!db_config.db_name || !db_config.db_user || !db_config.db_host || !db_config.db_password) {
  console.warn('Please configure db parameters `db_config.js`.');
}

app.engine('hbs', hbs({extname:'hbs', defaultLayout:'layout', layoutsDir: __dirname + '/views/layouts'}));

// view engine setup
app.set('views', path.join(__dirname, 'views'));
app.set('view engine', 'hbs');

// uncomment after placing your favicon in /public
//app.use(favicon(path.join(__dirname, 'public', 'favicon.ico')));
app.use(logger('dev'));
app.use(bodyParser.json());
app.use(bodyParser.urlencoded({ extended: false }));
app.use(cookieParser());
app.use(express.static(path.join(__dirname, 'public')));

app.use('/', routes);
app.use('/api', api_routes);
app.use('/bot', bot_routes);
app.use('/learn', learn);
// catch 404 and forward to error handler
app.use(function(req, res, next) {
  var err = new Error('Not Found');
  err.status = 404;
  next(err);
});

// error handlers

// development error handler
// will print stacktrace
if (app.get('env') === 'development') {
  app.use(function(err, req, res, next) {
    console.warn(err.stack);
    res.status(err.status || 500);
    res.render('error', {
      message: err.message,
      error: err
    });
  });
}

// production error handler
// no stacktraces leaked to user
app.use(function(err, req, res, next) {
  res.status(err.status || 500);
  res.render('error', {
    message: err.message,
    error: {}
  });
});


module.exports = app;
