var express = require('express');
var router = express.Router();
var config = require('../config');
var async = require('async');
var synaptic = require('synaptic'); // this line is not needed in the browser
var Neuron = synaptic.Neuron,
    Layer = synaptic.Layer,
    Network = synaptic.Network,
    Trainer = synaptic.Trainer,
    Architect = synaptic.Architect;
var NLP = require('stanford-corenlp');
var config = {"nlpPath":"../corenlp","version":"3.6.0"};
var coreNLP = new NLP.StanfordNLP({
 		   "nlpPath":"./corenlp",
		    "version":"3.6.0",
		    'annotators': ['tokenize','cleanxml','ssplit','pos', 'lemma','ner','regexner', 'parse','sentiment','depparse','quote'], //optional!
    //you can skip language if you want to use default english.

		},function(err) {
			console.log("DONE");
		});
/* GET home page. */
router.post('/', function(req, res) {
    var sentence = req.body.sentence;
		console.log(JSON.stringify(req.body));
 		coreNLP.process(sentence, function(err, result) {
		    	res.json({result: result});
		    	res.end();
		   });
});

function Perceptron(input, hidden, output)
{
    // create the layers
    var inputLayer = new Layer(input);
    var hiddenLayer = new Layer(hidden);
    var outputLayer = new Layer(output);

    // connect the layers
    inputLayer.project(hiddenLayer);
    hiddenLayer.project(outputLayer);

    // set the layers
    this.set({
        input: inputLayer,
        hidden: [hiddenLayer],
        output: outputLayer
    });
}

// extend the prototype chain
Perceptron.prototype = new Network();
Perceptron.prototype.constructor = Perceptron;

module.exports = router;
