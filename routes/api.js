var express = require('express');
var Api = require('../api/Api')

var router = express.Router();
var api = new Api();

router.get('/genre/:genre?/:limit?', function(req, res, next) {
  
  var genre = req.params.genre || 'action';
  var limit = req.params.limit || 3;
  
  api.getGenreBooks(genre, limit).then(function(data) {
      res.json(data);
  });
  
});

router.get('/authors/:author/:limit?', function(req, res, next) {
  
  var author = req.params.author;
  var limit = req.params.limit || 3;
  
  api.getAuthorBooks(author, limit).then(function(data) {
      res.json(data);
  });

});

router.get('/books/:title/:limit?', function(req, res, next) {
  
  var limit = req.params.limit || 5;
  var title = req.params.title;

  api.getBook(title, limit).then(function(data){
      res.json(data);
  });

});

// for debugging purposes
router.get('/user/:user_messenger_id', function(req, res, next) {
  var user_messenger_id = req.params.user_messenger_id;
  api.getUser(user_messenger_id).then(function(user) {
    res.json(user);
  });
});

router.get('/fulltext/:search_phrase', function(req, res, next) {
  var search_phrase = req.params.search_phrase;
  api.fullTextSearch(search_phrase).then(function(result) {
    res.json(result);
  });
});

module.exports = router;
