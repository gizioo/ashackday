var util = require('util');
var when = require('when');
var express = require('express');
var request = require('request');
var R = require('ramda');
var router = express.Router();
var config = require('../config');
var local_config = require('../local_config');
var IncomingMessage = require('../bot/IncomingMessage');
var IncomingMessageType = require('../bot/IncomingMessageType');
var Bot = require('../bot/Bot');


var bot = new Bot();

router.get('/', function(req, res) {
  if (req.query['hub.verify_token'] === config.verify_token) {
    res.send(req.query['hub.challenge']);
  } else {
    res.send('Invalid token');
  }
});

router.post('/', function(req, res) {
  var messaging_events = req.body.entry[0].messaging;
  console.log(util.inspect(messaging_events, {depth: 7}));
  for (var i = 0; i < messaging_events.length; i++) {
    try {
      var incoming_message = decodeIncomingMessage(messaging_events[i]);
      bot.getResponse(incoming_message).then(function(response) {
        if (response) {
          // run promises in sequence
          var promise = when.resolve();
          response.forEach(function(message) {
            promise = promise.then(function() {
              return sendMessage(message);
            });
          });
        }
      });
    } catch (err) {
      console.warn(err.message, err.stack);
    }
  }
  res.sendStatus(200);
});

/**
 * @param {*} fb_messaging_event
 * @returns {IncomingMessage}
 * @throws {Error} on unhandled message
 */
function decodeIncomingMessage(fb_messaging_event) {
  var event = fb_messaging_event;
  var sender = event.sender.id;
  if (event.message) {
    if (event.message.text) {
      return new IncomingMessage(IncomingMessageType.TEXT, sender, event.message.text);
    } else if (event.message.attachments) {
      var get_location = R.path([0, 'payload', 'coordinates']);
      if (get_location(event.message.attachments)) {
        var coordinates = get_location(event.message.attachments);
        return new IncomingMessage(IncomingMessageType.LOCATION, sender, coordinates);
      }
    }
  } else if (event.delivery) {
    return new IncomingMessage(IncomingMessageType.DELIVERY_CONFIRMATION, sender, event.delivery);
  } else if (event.postback) {
    return new IncomingMessage(IncomingMessageType.POSTBACK, sender, event.postback.payload);
  } else if (event.optin) {
    return new IncomingMessage(IncomingMessageType.AUTHENTICATION, sender, event.optin.ref);
  }

  throw new Error('Unhandled message ' + JSON.stringify(fb_messaging_event));
}

/**
 * @param {AbstractResponse} response
 */
function sendMessage(response) {
  return when.promise(function(resolve, reject) {
    request({
      url: 'https://graph.facebook.com/v2.6/me/messages',
      qs: {
        access_token: local_config.page_access_token
      },
      method: 'POST',
      json: response.getMessageData()
    }, function(error, response, body) {
      if (error) {
        return reject(error);
      } else if (response.body.error) {
        return reject(new Error('Error: ' + response.body.error));
      }
      return resolve(response);
    });
  });
}

module.exports = router;
