var express = require('express');
var router = express.Router();
var local_config = require('../local_config');

/* GET home page. */
router.get('/', function(req, res, next) {
  var context = {
    title: 'Book store',
    app_id: local_config.app_id,
    page_id: local_config.page_id
  };
  if (req.query.access_token) {
    context.access_token = req.query.access_token;
  }
  res.render('home', context);
});

module.exports = router;
