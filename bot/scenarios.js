var when = require('when');
var R = require('ramda');
var request = require('request');
var IncomingMessageType = require('./IncomingMessageType');
var TextResponse = require('./responses/TextResponse');
var ButtonsResponse = require('./responses/ButtonsResponse');
var GenericTemplateResponse = require('./responses/GenericTemplateResponse');
var response_helpers = require('./responses/helpers');
var questions = require('../api/Questions');
var Api = require('../api/Api');
var nlp = require('./nlp');

var api = new Api();

/**
 * @constructor
 * @param {number} id
 * @param {Step[]} steps
 */
var Scenario = function(id, steps) {
  this.id = id;
  this.steps = steps;
};

/**
 * @constructor
 * @param {number} id
 * @param {Object -> AbstractResponse[]} getMessages function that generates
 *        list of responses based on given user
 * @param {(Object, IncomingMessage) -> {scenario: number, step: number}} handleResponse
 *        returns object with scenario and step ids based on message returned by
 *        the step's `getMessages` function
 */
var Step = function(id, getMessages, handleResponse) {
  this.id = id;
  this.getMessages = getMessages;
  this.handleResponse = when.lift(handleResponse);
};

/**
 * @param {Object} user
 * @returns {number} scenario id
 */
function chooseNextScenario(user, current_scenario_id) {
  if (current_scenario_id) {
    var ranked_scenarios = user.ranked_scenarios;
    var current_scenario_index = ranked_scenarios.indexOf(scenario_id_to_label(current_scenario_id));
    var next_scenario_index = current_scenario_index + 1;
    if (next_scenario_index < ranked_scenarios.length) {
      return scenario_label_to_id(ranked_scenarios[next_scenario_index]);
    } else {
      return FREETALK_SCENARIO;
    }
  } else {
    return scenario_label_to_id(user.ranked_scenarios[0]);
  }
}

var rankedScenarios = when.lift(function(user) {
  return Object.keys(user.user_rank)
    .map(function(rank_name) {
      return [rank_name, user.user_rank[rank_name].sum];
    })
    .sort(function(rank_a, rank_b) {
      return rank_b[1] - rank_a[1];
    })
    .map(function(rank_pair) {
      return rank_pair[0];
    });
});

/**
 * @param {string} text
 * @returns {(Boolean|undefined)}
 */
function parseYesNo(text) {
  // TODO: ce with nlp
  var text = text.trim().toLowerCase();
  var promise = when.promise(function(resolve, reject, notify) {
      if (['y', 'yes', 'yep', 'yup', 'yeah', 'yea'].indexOf(text) >= 0) {
        return resolve(true);
      } else if (['n', 'no', 'nope'].indexOf(text) >= 0) {
        return resolve(false);
      } else {
        return resolve(undefined);
      }
  });
  return promise;
}

var scenarios = [];

// "People" scenario
var PEOPLE_SCENARIO = 1;
scenarios[PEOPLE_SCENARIO] = new Scenario(PEOPLE_SCENARIO, []);

scenarios[PEOPLE_SCENARIO].steps[0] = new Step(
  0,
  function(user, incoming_message) {
    return questions
      .getUserFamily(user.user_msg_id)
      .then(function(family) {
        var message_text = '';
        if (family.length === 0) {
          message_text = 'Are you looking for a present for someone in your family?';
        } else if (family.length > 1 && family.length <= 3 ) {
          var name = (family[0].name).split(' ');
          message_text = 'Are you looking for a present for your ' +
            family[0].relationship + ' ' + name[0] + ' or maybe for ' +
            'your ' + family[1].relationship + ' ' + family[1].name + '?';
        }else if(family.length > 3 || family.length === 1){
          var name = (family[0].name).split(' ');
          message_text = 'Are you looking for a present for your ' +
            family[0].relationship + ' ' + name[0] + '?';
        }
        return [new TextResponse(user.user_msg_id, message_text)];
      });
  },
  function(user, incoming_message) {
    if (incoming_message.type === IncomingMessageType.TEXT) {
      return parseYesNo(incoming_message.content).then(function(answer) {
        return questions
          .getUserCommonFriend(user.user_msg_id)
          .then(function(obj) {
            var has_common_friend = Boolean(obj);
            if (answer === true) {
              return {scenario: PEOPLE_SCENARIO, step: 1};
            } else if (answer === false) {
              if (has_common_friend) {
                return {scenario: PEOPLE_SCENARIO, step: 3};
              } else {
                var next_stream = chooseNextScenario(user, PEOPLE_SCENARIO);
                return {scenario: next_stream, step: 0};
              }
            } else {
              return {scenario: FREETALK_SCENARIO, step: 0};
            }
          });
      });
    }
    return {scenario: FREETALK_SCENARIO, step: 0};
  }
);

scenarios[PEOPLE_SCENARIO].steps[1] = new Step(
  1,
  function(user, incoming_message) {

    var buttons = [response_helpers.makePostbackButton('Buy', 'BUY_BOOK'),
                   response_helpers.makePostbackButton('No thanks', 'CONTINUE'),
                 response_helpers.makePostbackButton('More...', 'MORE')];   
    var buttons_nomore = [];
    buttons_nomore.push(buttons[0]);  
    buttons_nomore.push(buttons[1]);           
    if(incoming_message.content == 'MORE'){
        return returnPropositions(user, 'nature', 3, false, '', buttons_nomore);
    } 

    if (user.user_info.hasOwnProperty('family')) {
      if(user.user_info.family.data[0].relationship == 'mother'){
        return returnPropositions(user, 'kitchen', 3, false, '', buttons);
      }
      else if (user.user_info.family.data[0].relationship == 'daughter') {
        return returnPropositionsThree(user, ['cartoon','cats','teen'], 4, false, '', buttons);
      }
      else if (user.user_info.family.data[0].relationship == 'father') {
        return returnPropositions(user, 'cars', 3, false, '', buttons);
      }
      else if (user.user_info.family.data[0].relationship == 'sister') {
        return returnPropositions(user, 'music', 3, false, '', buttons);
      }
      else if (user.user_info.family.data[0].relationship == 'brother') {
        return returnPropositions(user, 'football', 3, false, '', buttons);
      }
    }
    return returnPropositions(user, 'travel', 3, false, '', buttons);
  },
  function(user, incoming_message) {
    switch (incoming_message.type) {
    case IncomingMessageType.POSTBACK:
      if (incoming_message.content == 'BUY_BOOK') {
        return {scenario: PEOPLE_SCENARIO, step: 2};
      } else if (incoming_message.content == 'CONTINUE') {
        return questions
          .getUserCommonFriend(user.user_msg_id)
          .then(function(obj) {
            if (obj) {
              return {scenario: PEOPLE_SCENARIO, step: 3};
            } else {
              var next_stream = chooseNextScenario(user, PEOPLE_SCENARIO);
              return {scenario: next_stream, step: 0};
            }
          });
      }else if(incoming_message.content == 'MORE'){
          return {scenario: PEOPLE_SCENARIO, step: 1};
      }  
      break;
    case IncomingMessageType.TEXT:
      return parseYesNo(incoming_message.content).then(function(answer) {
        return questions
          .getUserCommonFriend(user.user_msg_id)
          .then(function(obj) {
            if (answer === false && obj) {
              return {scenario: PEOPLE_SCENARIO, step: 3};
            } else {
              return {scenario: FREETALK_SCENARIO, step: 0};
            }
          });
      });
      break;
    default:
      return {scenario: FREETALK_SCENARIO, step: 0};
    }
  }
);

scenarios[PEOPLE_SCENARIO].steps[2] = new Step(
  2,
  function(user, incoming_message) {
    return [new TextResponse(user.user_msg_id, 'SALE')];
  },
  function(user, incoming_message) {
    return {scenario: FREETALK_SCENARIO, step: 0};
  }
);

scenarios[PEOPLE_SCENARIO].steps[3] = new Step(
  3,
  function(user, incoming_message) {
    var buttons = [response_helpers.makePostbackButton('Buy', 'BUY_BOOK'),
                   response_helpers.makePostbackButton('No thanks', 'CONTINUE'),
                 response_helpers.makePostbackButton('More...', 'MORE')];   
    var buttons_nomore = [];
    buttons_nomore.push(buttons[0]);  
    buttons_nomore.push(buttons[1]);           
    if(incoming_message.content == 'MORE'){
        return returnPropositions(user, 'nature', 3, false, '', buttons_nomore);
    } 
    return questions
      .getUserCommonFriend(user.user_msg_id)
      .then(function(obj) {
        var friend_name = (obj) ? obj.person.name : '';
        var message_text = '';
        if (friend_name) {
          var splitted_friend_name = friend_name.split(' ');
          message_text = 'You spend time with your friends.\n' +
            'Maybe something interesting for ' + splitted_friend_name[0] + '?';
        } else {
          message_text = 'Are you thinking about buying something for your best friend?';
        } 
        return returnPropositions(user, 'cooking', 3, true, message_text, buttons);
      });
  },
  function(user, incoming_message) {
    switch (incoming_message.type) {
    case IncomingMessageType.POSTBACK:
      if (incoming_message.content == 'BUY_BOOK') {
        return {scenario: PEOPLE_SCENARIO, step: 2};
      } else if (incoming_message.content == 'CONTINUE') {
        return questions
          .isUserInRelationship(user.user_msg_id)
          .then(function(in_relationship) {
            if (in_relationship) {
              return {scenario: PEOPLE_SCENARIO, step: 4};
            } else {
              var next_stream = chooseNextScenario(user, PEOPLE_SCENARIO);
              return {scenario: next_stream, step: 0};
            }
          });
      }else if (incoming_message.content == 'MORE') {
          return {scenario: PEOPLE_SCENARIO, step: 3};
      }
      break;
    case IncomingMessageType.TEXT:
      return parseYesNo(incoming_message.content).then(function(answer) {
        return questions
          .isUserInRelationship(user.user_msg_id)
          .then(function(in_relationship) {
            if (answer === false && in_relationship) {
              return {scenario: PEOPLE_SCENARIO, step: 4};
            } else {
              var next_stream = chooseNextScenario(user, PEOPLE_SCENARIO);
              return {scenario: next_stream, step: 0};
            }
          });
      });
      break;
    default:
      return {scenario: FREETALK_SCENARIO, step: 0};
    }
  }
);

scenarios[PEOPLE_SCENARIO].steps[4] = new Step(
  4,
  function(user, incoming_message) {
    var message = 'You are in a relationship. Here are some inspirations <3';
    var base_buttons = [
      response_helpers.makePostbackButton('Buy', 'BUY_BOOK'),
      response_helpers.makePostbackButton('No thanks', 'CONTINUE')
    ];
    var all_buttons = base_buttons.concat([
      response_helpers.makePostbackButton('More...', 'MORE')
    ]);
    if (incoming_message.content == 'MORE') {
      return returnPropositions(user, 'cooking', 3, false, message, base_buttons);
    } else {
      return returnPropositions(user, 'art', 3, true, message, all_buttons);
    }
  },
  function(user, incoming_message) {
    switch (incoming_message.type) {
    case IncomingMessageType.POSTBACK:
      if (incoming_message.content == 'BUY_BOOK') {
        return {scenario: PEOPLE_SCENARIO, step: 2};
      } else if (incoming_message.content == 'MORE') {
        return {scenario: PEOPLE_SCENARIO, step: 4};
      } else if (incoming_message.content == 'CONTINUE') {
        var next_stream = chooseNextScenario(user, PEOPLE_SCENARIO);
        return {scenario: next_stream, step: 0};
      }
      break;
    case IncomingMessageType.TEXT:
      var next_stream = chooseNextScenario(user, PEOPLE_SCENARIO);
      return {scenario: next_stream, step: 0};
    default:
      return {scenario: FREETALK_SCENARIO, step: 0};
    }
  }
);
// "People" scenario end


function locationQ1(user, incoming_message, location) {
  return [
    new ButtonsResponse(
      incoming_message.sender,
      'Are you still in ' + location + '? Maybe a tourist guide?',
      [
        response_helpers.makePostbackButton('Yes', 'YES_TOURIST_GUIDE'),
        response_helpers.makePostbackButton('No', 'CONTINUE')
      ]
    )
  ];
}

/* Temporary not used
function locationQ2(user, incoming_message, location) {
  return [
    new ButtonsResponse(
      incoming_message.sender,
      'I see you like ' + location + '. Maybe a guide about that place?',
      [
        response_helpers.makePostbackButton('Yes', 'YES_REGION'),
        response_helpers.makePostbackButton('No', 'CONTINUE')
      ]
    )
  ];
}
*/

function getBooksForLocation(user, location) {
  var title = [];
  var img = [];
  var author = [];
  var buttons = [response_helpers.makePostbackButton('Buy', 'BUY_BOOK'),
                 response_helpers.makePostbackButton('No thanks', 'CONTINUE')];
  return api.fullTextSearchOrderByYear(location).then(function(data) {
    if((data.rows).length > 0){
      data.rows.forEach(function(elem) {
        if(['travel', 'guidebook'].indexOf(elem.b_genre) >= 0){
          title.push(elem.b_title);
          img.push(elem.b_img);
          author.push(elem.b_author);
        }  
      });
    }
  }).then(function() {
    var results = [];
    for (var i = 0; i < title.length; i++) {
      results.push(response_helpers.makeGenericTemplateElement(title[i],img[i],author[i],buttons))
    }
    if (results.length > 0) {
      return [new GenericTemplateResponse(user.user_msg_id, results)];
    } else {
      return [
        new ButtonsResponse(
          user.user_msg_id,
          'Sorry, we couldn\'t find any books matching this query.',
          [response_helpers.makePostbackButton('No thanks', 'CONTINUE')]
        )];
    }
  });
}

function locationQ3(user, incoming_message, locations) {
  return [
    new ButtonsResponse(
      incoming_message.sender,
      'You love travelling: ' + locations.join(', ') + '. Maybe search for a new destination?',
      [
        response_helpers.makePostbackButton('Yes', 'YES_NEW_DESTINATION'),
        response_helpers.makePostbackButton('No', 'CONTINUE')
      ]
    )
  ];
}

// "Location" scenario
var LOCATION_SCENARIO = 2;
scenarios[LOCATION_SCENARIO] = new Scenario(LOCATION_SCENARIO, []);
// placeholder scenario
scenarios[LOCATION_SCENARIO].steps[0] = new Step(
  0,
  function(user, incoming_message) {
    if (user.user_rank.location.q1) {
      return questions.getUserCurrentLocation(user.user_msg_id)
        .then(function(locations) {
          return locationQ1(user, incoming_message, locations[0]);
        });
    } 
    /*
    else if (user.user_rank.location.q2) {
      return questions.getUserFavoritePlace(user.user_msg_id)
        .then(function(location) {
          return locationQ2(user, incoming_message, location);
        });
    } 
    */
    else if (true || user.user_rank.location.q3) {
      return questions.getUserVisitedLocations(user.user_msg_id)
        .then(function(locations) {
          return locationQ3(user, incoming_message, locations);
        });
    }
    return [new TextResponse(incoming_message.sender, 'Location scenario')];
  },
  function(user, incoming_message) {
    switch (incoming_message.type) {
    case IncomingMessageType.POSTBACK:
      if (incoming_message.content =='YES_TOURIST_GUIDE') {
        return {scenario: LOCATION_SCENARIO, step: 1};
      } else if (incoming_message.content == 'YES_REGION') {
        return {scenario: LOCATION_SCENARIO, step: 4};
      } else if (incoming_message.content == 'YES_NEW_DESTINATION') {
        return {scenario: LOCATION_SCENARIO, step: 6};
      } else if (incoming_message.content == 'CONTINUE') {
        if (!(!user.user_rank.location.q1 &&
              user.user_rank.location.q2)) {
          return {scenario: LOCATION_SCENARIO, step: 3};
        } else if (!(!user.user_rank.location.q1 &&
                   !user.user_rank.location.q2 &&
                     user.user_rank.location.q3)) {
          return {scenario: LOCATION_SCENARIO, step: 5};
        }
      }
      break;
    }
    return {scenario: FREETALK_SCENARIO, step: 0};
  }
);

scenarios[LOCATION_SCENARIO].steps[1] = new Step(
  1,
  function(user, incoming_message) {
    return questions.getUserCurrentLocation(user.user_msg_id)
      .then(function(locations) {
        return getBooksForLocation(user, locations[0]);
      });
  },
  function(user, incoming_message) {
    switch (incoming_message.type) {
    case IncomingMessageType.POSTBACK:
      if (incoming_message.content =='BUY_BOOK') {
        return {scenario: LOCATION_SCENARIO, step: 2};
      } else if (incoming_message.content == 'CONTINUE') {
        if (user.user_rank.location.q1 && user.user_rank.location.q2) {
          return {scenario: LOCATION_SCENARIO, step: 3};
        } else if (user.user_rank.location.q3) {
          return {scenario: LOCATION_SCENARIO, step: 5};
        } else {
          var next_stream = chooseNextScenario(user, LOCATION_SCENARIO);
          return {scenario: next_stream, step: 0};
        }
      }
      break;
    }
    return {scenario: FREETALK_SCENARIO, step: 0};
  }
);

scenarios[LOCATION_SCENARIO].steps[2] = new Step(
  2,
  function(user, incoming_message) {
    return [new TextResponse(user.user_msg_id, 'SALE')];
  },
  function(user, incoming_message) {
    return {scenario: FREETALK_SCENARIO, step: 0};
  }
);

scenarios[LOCATION_SCENARIO].steps[3] = new Step(
  3,
  function(user, incoming_message) {
    /*
    if (user.user_rank.location.q2) {
      return questions.getUserFavoritePlace(user.user_msg_id)
        .then(function(location) {
          return locationQ2(user, incoming_message, location);
        });
    } else 
    */
    if (user.user_rank.location.q3) {
      return questions.getUserVisitedLocations(user.user_msg_id)
        .then(function(locations) {
          return locationQ3(user, incoming_message, locations);
        });
    }
    return [];
  },
  function(user, incoming_message) {
    switch (incoming_message.type) {
    case IncomingMessageType.POSTBACK:
      if (incoming_message.content == 'YES_REGION') {
        return {scenario: LOCATION_SCENARIO, step: 4};
      } else if (incoming_message.content == 'YES_NEW_DESTINATION') {
        return {scenario: LOCATION_SCENARIO, step: 6};
      } else if (incoming_message.content == 'CONTINUE') {
        if (user.user_rank.location.q3) {
          return {scenario: LOCATION_SCENARIO, step: 5};
        } else {
          var next_stream = chooseNextScenario(user, LOCATION_SCENARIO);
          return {scenario: next_stream, step: 0};
        }
      }
      break;
    }
    return {scenario: FREETALK_SCENARIO, step: 0};
  }
);

scenarios[LOCATION_SCENARIO].steps[4] = new Step(
  4,
  function(user, incoming_message) {
    return questions.getUserFavoritePlace(user.user_msg_id)
      .then(function(location) {
        return getBooksForLocation(user, location);
      });
  },
  function(user, incoming_message) {
    switch (incoming_message.type) {
    case IncomingMessageType.POSTBACK:
      if (incoming_message.content =='BUY_BOOK') {
        return {scenario: LOCATION_SCENARIO, step: 2};
      } else if (incoming_message.content == 'CONTINUE') {
        if (user.user_rank.location.q3) {
          return {scenario: LOCATION_SCENARIO, step: 5};
        } else {
          var next_stream = chooseNextScenario(user, LOCATION_SCENARIO);
          return {scenario: next_stream, step: 0};
        }
      }
      break;
    }
    return {scenario: FREETALK_SCENARIO, step: 0};
  }
);

scenarios[LOCATION_SCENARIO].steps[5] = new Step(
  5,
  function(user, incoming_message) {
    return questions.getUserVisitedLocations(user.user_msg_id)
      .then(function(locations) {
        return locationQ3(user, incoming_message, locations);
      });
  },
  function(user, incoming_message) {
    switch (incoming_message.type) {
    case IncomingMessageType.POSTBACK:
      if (incoming_message.content == 'YES_NEW_DESTINATION') {
        return {scenario: LOCATION_SCENARIO, step: 6};
      } else if (incoming_message.content == 'CONTINUE') {
        var next_stream = chooseNextScenario(user, LOCATION_SCENARIO);
        return {scenario: next_stream, step: 0};
      }
      break;
    }
    return {scenario: FREETALK_SCENARIO, step: 0};
  }
);

scenarios[LOCATION_SCENARIO].steps[6] = new Step(
  6,
  function(user, incoming_message) {
    return api.getGenreBooks('guidebook', 3).then(function(books) {
      var book_elements = books.map(function(book) {
        return response_helpers.makeGenericTemplateElement(
          book.book_title,
          book.book_img_url,
          book.book_author,
          [
            response_helpers.makePostbackButton('Buy', 'BUY_BOOK'),
            response_helpers.makePostbackButton('No thanks', 'CONTINUE')
          ]
        );
      });
      return [new GenericTemplateResponse(user.user_msg_id, book_elements)];
    });
  },
  function(user, incoming_message) {
    switch (incoming_message.type) {
    case IncomingMessageType.POSTBACK:
      if (incoming_message.content =='BUY_BOOK') {
        return {scenario: LOCATION_SCENARIO, step: 2};
      } else if (incoming_message.content == 'CONTINUE') {
        var next_stream = chooseNextScenario(user, LOCATION_SCENARIO);
        return {scenario: next_stream, step: 0};
      }
      break;
    }
    return {scenario: FREETALK_SCENARIO, step: 0};
  }
);
// "Location" scenario end


// "Interest" scenario
var INTEREST_SCENARIO = 3;
scenarios[INTEREST_SCENARIO] = new Scenario(INTEREST_SCENARIO, []);
// placeholder scenario
scenarios[INTEREST_SCENARIO].steps[0] = new Step(
  0,
  function(user, incoming_message) {
    var liked_authors = getLikedAuthors(user);
    if (liked_authors && liked_authors.length > 0) {
      return [
        new ButtonsResponse(
          incoming_message.sender,
          'I see you like ' + liked_authors.join(', ') +
            '. Would you like more from this author(s)?',
          [
            response_helpers.makePostbackButton('Yes', 'YES_AUTHORS'),
            response_helpers.makePostbackButton('No thanks', 'CONTINUE')
          ]
        )
      ];
    } else {
      //
      return [
        new ButtonsResponse(
          incoming_message.sender,
          'I see you like music. Would you like some books about it?',
          [
            response_helpers.makePostbackButton('Yes', 'YES_MUSIC'),
            response_helpers.makePostbackButton('No thanks', 'CONTINUE')
          ]
        )
      ];
    }
    return [new TextResponse(incoming_message.sender, 'Interest scenario')];
  },
  function(user, incoming_message) {
    switch (incoming_message.type) {
    case IncomingMessageType.POSTBACK:
      if (incoming_message.content == 'YES_AUTHORS') {
        return {scenario: INTEREST_SCENARIO, step: 1};
      } else if (incoming_message.content == 'YES_MUSIC') {
        return {scenario: INTEREST_SCENARIO, step: 2};
      } else if (incoming_message.content == 'CONTINUE') {
        var next_stream = chooseNextScenario(user, INTEREST_SCENARIO);
        return {scenario: next_stream, step: 0};
      }
      break;
    }
    return {scenario: FREETALK_SCENARIO, step: 0};
  }
);

scenarios[INTEREST_SCENARIO].steps[1] = new Step(
  1,
  function(user, incoming_message) {
    var liked_authors = getLikedAuthors(user);
    var books_promises = liked_authors.map(function(author) {
      return api.getAuthorBooks(author, 3);
    });
    var book_element_promises = books_promises.map(function(books) {
      return books.map(function(book) {
        return response_helpers.makeGenericTemplateElement(
          book.book_title,
          book.book_img_url,
          book.book_author,
          [
            response_helpers.makePostbackButton('Buy', 'BUY_BOOK'),
            response_helpers.makePostbackButton('No thanks', 'CONTINUE')
          ]
        );
      });
    });
    return when.all(book_element_promises).then(function(book_elements) {
      var book_elements = R.flatten(book_elements);
      if (book_elements.length > 0) {
        return [new GenericTemplateResponse(user.user_msg_id, book_elements)];
      } else {
        return [
          new ButtonsResponse(
            user.user_msg_id,
            'Sorry, we couldn\'t find any books matching this query.',
            [response_helpers.makePostbackButton('Try something else...', 'CONTINUE')]
          )];
      }
    });
  },
  function(user, incoming_message) {
    switch (incoming_message.type) {
    case IncomingMessageType.POSTBACK:
      if (incoming_message.content == 'BUY_BOOK') {
        return {scenario: INTEREST_SCENARIO, step: 3};
      } else if (incoming_message.content == 'CONTINUE') {
        var next_stream = chooseNextScenario(user, INTEREST_SCENARIO);
        return {scenario: next_stream, step: 0};
      }
      break;
    }
    return {scenario: FREETALK_SCENARIO, step: 0};
  }
);

scenarios[INTEREST_SCENARIO].steps[2] = new Step(
  2,
  function(user, incoming_message) {
    return api.getGenreBooks('music', 3)
      .then(function(books) {
        var book_elements = books.map(function(book) {
          return response_helpers.makeGenericTemplateElement(
            book.book_title,
            book.book_img_url,
            book.book_author,
            [
              response_helpers.makePostbackButton('Buy', 'BUY_BOOK'),
              response_helpers.makePostbackButton('No thanks', 'CONTINUE')
            ]
          );
        });
        return [new GenericTemplateResponse(user.user_msg_id, book_elements)];
      });
  },
  function(user, incoming_message) {
    switch (incoming_message.type) {
    case IncomingMessageType.POSTBACK:
      if (incoming_message.content == 'BUY_BOOK') {
        return {scenario: INTEREST_SCENARIO, step: 3};
      } else if (incoming_message.content == 'CONTINUE') {
        var next_stream = chooseNextScenario(user, INTEREST_SCENARIO);
        return {scenario: next_stream, step: 0};
      }
      break;
    }
    return {scenario: FREETALK_SCENARIO, step: 0};
  }
);

scenarios[INTEREST_SCENARIO].steps[3] = new Step(
  3,
  function(user, incoming_message) {
    return [new TextResponse(user.user_msg_id, 'SALE')];
  },
  function(user, incoming_message) {
    return {scenario: FREETALK_SCENARIO, step: 0};
  }
);
// "Interest" scenario end

// Freetalk scenario
var FREETALK_SCENARIO = 10;
scenarios[FREETALK_SCENARIO] = new Scenario(FREETALK_SCENARIO, []);

scenarios[FREETALK_SCENARIO].steps[0] = new Step(
  0,
  function(user, incoming_message) {
    if (incoming_message.type == IncomingMessageType.TEXT) {
      var title = [];
      var img = [];
      var author = [];
  
      var buttons = [response_helpers.makePostbackButton('Yes', 'YES_REGION'),
                 response_helpers.makePostbackButton('No', 'CONTINUE')];   

      return nlp.getNLP(incoming_message.content).then(function(res) {
	if(res.action == "search"){
	   var nea = [];
	   if(res.named_entities){
		for(var i in res.named_entities){
			nea.push(res.named_entities[i]);	
		}
	   }
	   if(res.type){
		nea.push(res.type);
		buttons = [response_helpers.makePostbackButton('Buy', 'ZANOX_'+res.type),
                 		response_helpers.makePostbackButton('No', 'CONTINUE')];   



	   }
	   if(res.specific){
		 nea.push(res.specific);
	   }
	   var ua = nea.filter(function(elem, pos) {
    		return nea.indexOf(elem) == pos;
	   }); 
	   var sp = nea.join(" ");
 	   return api.fullTextSearch(sp).then(function(data) {
		
               if((data.rows).length > 0){
		 var i = 0;
                 data.rows.forEach(function(elem) {
		   if(i<=5){
                      title.push(elem.b_title);
                      img.push(elem.b_img);
                      author.push(elem.b_author);
		      i+=1;
		   }
                 });
               };
           }).then(function() {
               var results = [];
               for (var i = 0; i < title.length; i++) {
                   results.push(response_helpers.makeGenericTemplateElement(title[i],img[i],author[i],buttons))
               }
               return [new GenericTemplateResponse(user.user_msg_id, results)];
           });
         }else{
	       return [new TextResponse(user.user_msg_id, "I'm sorry; I’m not sure I understand.")];
		
	}
      })
      .catch(function(error) {
        console.warn(error.message, error.stack);
        return [new TextResponse(incoming_message.sender, 'NLP error')];
      });
    }else{
	return [new TextResponse(user.user_msg_id, "What are you looking for?")];
    }
    return [];
  },
  function(user, incoming_message) {
    switch (incoming_message.type) {
    case IncomingMessageType.POSTBACK:
      return {scenario: FREETALK_SCENARIO, step: 1};
      break;
    default:
      return {scenario: FREETALK_SCENARIO, step: 0};
    }
  }
);
var getZANOX = function(q){
    var that = this;
    var promise = when.promise(function(resolve, reject, notify) {
         request.get({
                url: 'http://api.zanox.com/json/products?connectid=43EEF0445509C7205827&items=5&q='+q,
                method: 'GET'
                }, function(err, result, body){
                        return resolve(body);
                });
    });
    return promise;
}

scenarios[FREETALK_SCENARIO].steps[1] = new Step(
  1,
  function(user, incoming_message) {
    if (incoming_message.type == IncomingMessageType.POSTBACK) {
	if (incoming_message.content.lastIndexOf("ZANOX",0) === 0){
		var q = incoming_message.content.split("_")[1];
		return getZANOX(q).then(function(res){
				var data = JSON.parse(res);
				var productItem = data.productItems.productItem[0];
				var titles = [];
				var images = [];
				
				var results = [];
				var buttons = [response_helpers.makePostbackButton('Buy', 'YES')];   
				
				results.push(response_helpers.makeGenericTemplateElement(productItem.name, productItem.image.large || productItem.image.medium || productItem.image.small, productItem.price + " " + productItem.currency, buttons));
				return [new TextResponse(user.user_msg_id,"Thanks for the buy. Enjoy the book! As a bonus we have an useful offer for you from our beloved ZANOX shop."), new GenericTemplateResponse(user.user_msg_id, results)];
			});		
	}
    }
  },
  function(user, incoming_message){
    return {scenario: FREETALK_SCENARIO, step: 0};
  }
);

// Freetalk scenario end

/**
 * @param {number} id
 * @returns {string}
 */
function scenario_id_to_label(id) {
  switch(id) {
  case PEOPLE_SCENARIO:
    return 'people';
  case LOCATION_SCENARIO:
    return 'location';
  case INTEREST_SCENARIO:
    return 'interest';
  }
}

/**
 * @param {string} label
 * @returns {number}
 */
function scenario_label_to_id(label) {
  switch(label) {
  case 'people':
    return PEOPLE_SCENARIO;
  case 'location':
    return LOCATION_SCENARIO;
  case 'interest':
    return INTEREST_SCENARIO;
  }
}

function getLikedAuthors(user) {
  var likes = R.path(['user_info', 'likes', 'data'], user);
  if (likes) {
    var authors = likes.filter(function(like) {
      return like.category == 'Author';
    }).map(function(like) {
      return like.name;
    });
    return R.uniq(authors);
  }
  return [];
}

/*
*@param {object} user object
*@param {string} genre
Helper function returns 3 propositions
*/
function returnPropositions(user, genre, limit, text_response, message_text, buttons) {
  var title = [];
  var img = [];
  var author = [];
  return api.getGenreBooks(genre, 3).then(function(data){
    data.forEach(function(item) {
      title.push(item.book_title);
      img.push(item.book_img_url);
      author.push(item.book_author);
    });
    var results = [];
    for (var i = 0; i < limit; i++) {
      results.push(response_helpers.makeGenericTemplateElement(title[i],img[i],author[i],buttons))
    }
    if (text_response === true) {
      return [new TextResponse(user.user_msg_id, message_text), 
        new GenericTemplateResponse(user.user_msg_id, results.reverse())];
    }else{
      return [new GenericTemplateResponse(user.user_msg_id, results.reverse())];
    }
  });
}

function returnPropositionsThree(user, genre, limit, text_response, message_text, buttons) {
  var title = [];
  var img = [];
  var author = [];
  return api.getGenreBooksThree(genre, 4).then(function(data){
    data.forEach(function(item) {
      title.push(item.book_title);
      img.push(item.book_img_url);
      author.push(item.book_author);
    });
    
    var results = [];
    for (var i = 0; i < limit; i++) {
      if(i != 1)
        results.push(response_helpers.makeGenericTemplateElement(title[i],img[i],author[i],buttons))
    }
    if (text_response === true) {
      return [new TextResponse(user.user_msg_id, message_text), 
        new GenericTemplateResponse(user.user_msg_id, results.reverse())];
    }else{
      return [new GenericTemplateResponse(user.user_msg_id, results.reverse())];
    }
  });
}

module.exports = {
  Scenario: Scenario,
  Step: Step,
  scenarios: scenarios,
  chooseNextScenario: chooseNextScenario,
  FREETALK_SCENARIO: FREETALK_SCENARIO,
  rankedScenarios: rankedScenarios
};
