var when = require('when');
var request = require('request');
var graph = require('fbgraph');

module.exports = {
  queryGraphApi: queryGraphApi,
  getFbData: getFbData
};

/**
 * @param {string} user_access_token
 * @param {string[]} fields Array of fields to query for, optionally with
 *                   modifiers e.g. `likes`, `posts.limit(10)`
 * @returns {Promise.<Object>} graph api response
 */
function queryGraphApi(user_access_token, fields) {
  return when.promise(function(resolve, reject, notify) {
    request({
      url: 'https://graph.facebook.com/v2.6/me',
      qs: {
        fields: fields.join(','),
        access_token: user_access_token
      },
      method: 'GET'
    }, function(error, response, body) {
      if (error) {
        return reject(error);
      }
      var resp = JSON.parse(body);
      return resolve(resp);
    });
  });
}

/* Not in use right now*/
function getFbData(accessToken, callback) {
  graph.setAccessToken(accessToken);
  graph.get("me?fields=id,name,about,location,birthday,devices," +
            "hometown,relationship_status,likes,books,movies,music,family", function(err, res) {
      if (err) {
        console.error('Error user results');
      }
      else
        return callback(res);
  });
}
