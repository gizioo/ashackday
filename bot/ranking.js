var when = require('when');

var calcRank = when.lift(function (data) {
  
  var rank = {
    "people" : {
      "q1": 0,
      "q2": 0,
      "q3": 0,
      "sum": 0,
    },
    "interest" : {
      "q1": 0,
      "q2": 0,
      "q3": 0,
      "sum": 0
    },
    "location" : {
      "q1": 0,
      "q2": 0,
      "q3": 0,
      "sum": 0
    }
  };
  
  var family = []
  var friends_tagged = [];
  var count = 0;
  var cities = [];
  var home_location = [];
  var location_approve = 0;
  var all_visited_location = [];
  var common_city_occurence = 0;
  var splitted_home_locations = [];
  var friends = '';
  
  /*People*/
  //q1
  if(data.hasOwnProperty('family')){
    family.push(data.family.data[0].name);
    rank.people.q1 += 7;
  }
  //q2  
  if(data.hasOwnProperty('feed')){
    (data.feed.data).forEach(function(elem) {
      if(elem.hasOwnProperty('with_tags')){
        for (var i = 0; i < (elem.with_tags.data).length; i++) {
          friends += elem.with_tags.data[i].name + ',';
        }
      }
    });
    var friends_copy = friends.split(',');
    var friends_copy2 = friends_copy.slice();
    var length = friends_copy2.length;
    var common_friend = getMaxOccurrence(friends_copy2);
    if(family.indexOf(common_friend)==-1){
      friends_copy2.forEach(function(elem) {
        if(elem === common_friend)
          count += 1;
      });
      if((count/length)>0.10) {
        rank.people.q2 += 5;
      }

    }
    
  }

  //q3
  if(data.hasOwnProperty('relationship_status')){
    if(data.relationship_status === 'In a relationship' || 
       data.relationship_status === 'Married' || 
       data.relationship_status === 'Engaged')
      rank.people.q3 += 2;
  }
  
  /*Interest*/  
  //q1
  if(data.hasOwnProperty('likes')){
    var music_band = getCategory(data.likes.data, "Musician/Band");
    var book = getCategory(data.likes.data, "Book");
    var movie = getCategory(data.likes.data, "Movie");
    var author = getCategory(data.likes.data, "Author");
    var sum = music_band.length + book.length + movie.length + author.length;
    if(sum > 0)
      rank.interest.q1 += 7;
  }
  
  //q2
  if (data.hasOwnProperty('books')) {
    if((data.books.data).length>0)
      rank.interest.q2 += 4;
  }
  
  //q3
  if(data.hasOwnProperty('movies') || data.hasOwnProperty('music')){
    if ((data.movies && data.movies.data.length > 0) || (data.music && data.music.data.length > 0)) {
      rank.interest.q3 += 2;
    }
  }

  /*Location*/
  //q1
  if(data.hasOwnProperty('feed')){
    (data.feed.data).forEach(function(elem) {
      if(elem.hasOwnProperty('place')){
        if(elem.place.hasOwnProperty('location')){
          if(elem.hasOwnProperty('created_time')){
            var difference = new Date() - new Date(elem.created_time);
            if(milisecondToDays(difference)<= 0){
              cities.push(elem.place.location.city);
            }
          }
        }
      }
    });
    if(data.hasOwnProperty('location') || data.hasOwnProperty('hometown')){
      if(data.hasOwnProperty('location'))
        home_location.push(data.location.name);
      if(data.hasOwnProperty('hometown'))
        home_location.push(data.hometown.name);
      var splitted_home_locations = home_location.join(',').split(',');        
      cities.forEach(function(elem) {
        if(splitted_home_locations.indexOf(elem)==-1)
          location_approve += 1;
      });
      if(location_approve>0)
        rank.location.q1 += 5;
    }
  }  
  
  //q2
  if(data.hasOwnProperty('feed')){
    (data.feed.data).forEach(function(elem) {
      if(elem.hasOwnProperty('place')){
        if(elem.place.hasOwnProperty('location')){
            all_visited_location.push(elem.place.location.city);
        }
      }
    });
    var common_city = getMaxOccurrence(all_visited_location);
    all_visited_location.forEach(function(elem) {
      if (elem == common_city) {
        common_city_occurence += 1;
      }
    });
    if (splitted_home_locations.indexOf(common_city)==-1) {
      if (common_city_occurence/all_visited_location.length > 0.1) {
          rank.location.q2 += 3;
      }
    }
  }
  
  //q3
  if (all_visited_location.length>3) {
    rank.location.q3 += 2;
  }
  
  //Calc sum
  return setSum(rank).then(function(result){
    var rank = result;
    console.log(rank);
    return rank;
  });  
  
});

/*Helpers*/

var setSum = when.lift(function(rank){
  rank.people.sum = rank.people.q1 + rank.people.q2 + rank.people.q3;
  rank.interest.sum = rank.interest.q1 + rank.interest.q2 + rank.interest.q3;
  rank.location.sum = rank.location.q1 + rank.location.q2 + rank.location.q3;
  return rank;
}); 

function getCategory(data, category) {
  return data.filter(
      function(data){return data.category == category}
  );
}

function getMaxOccurrence(arr) {
    var o = {}, maxCount = 0, maxValue, m;
    for (var i=0, iLen=arr.length; i<iLen; i++) {
        m = arr[i];
        if (!o.hasOwnProperty(m)) {
            o[m] = 0;
        }
        ++o[m];
        if (o[m] > maxCount) {
            maxCount = o[m];
            maxValue = m;
        }
    }
    return maxValue;
}

function milisecondToDays(ms){
    return Math.floor(ms / (24*60*60*1000));
}

module.exports = {
  calcRank: calcRank,
  getMaxOccurrence : getMaxOccurrence,
  getCategory: getCategory,
  milisecondToDays: milisecondToDays
};
