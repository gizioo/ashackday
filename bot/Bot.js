var when = require('when');
var IncomingMessageType = require('./IncomingMessageType');
var ResponseType = require('./responses/ResponseType');
var TextResponse = require('./responses/TextResponse');
var ButtonsResponse = require('./responses/ButtonsResponse');
var GenericTemplateResponse = require('./responses/GenericTemplateResponse');
var responses_helpers = require('./responses/helpers');
var geolocation = require('./geolocation');
var request = require('request');
var Api = require('../api/Api')
var graph_api = require('./graph_api')
var categories = require('../categories');
var scenarios = require('./scenarios');

var api = new Api();
var cheats = ['iddqd', 'idkfa', 'restart'];
/**
 * @constructor
 */
var Bot = function() {};
/**
 * @param {IncomingMessage} incoming_message
 * @returns {Promise.<AbstractResponse[]|null>}
 */
Bot.prototype.getResponse = function(incoming_message) {
  switch (incoming_message.type) {
  case IncomingMessageType.TEXT:
  case IncomingMessageType.POSTBACK:
  case IncomingMessageType.LOCATION:
    return api.getUser(incoming_message.sender).then(function(user) {
      var state = user.state;
	console.log("STATE");
	console.log(state);
      if (!state || !state.scenario || cheats.indexOf(incoming_message.content) >= 0) {
        state = {};
        state.scenario = scenarios.chooseNextScenario(user);
        state.step = 0;
        return api.updateState(incoming_message.sender, JSON.stringify(state))
          .then(function(updated) {
            return scenarios.scenarios[state.scenario].steps[state.step].getMessages(user, incoming_message);
          });
      }
      return scenarios
        .scenarios[state.scenario]
        .steps[state.step]
        .handleResponse(user, incoming_message) // gets new state
        .then(function(next_state) {
	  console.log(next_state);
          return api.updateState(incoming_message.sender, JSON.stringify(next_state)) // saves state
            .then(function(updated) {
              return scenarios
                .scenarios[next_state.scenario]
                .steps[next_state.step]
                .getMessages(user, incoming_message)
            });
        });
    });

    return when.resolve(null);
  case IncomingMessageType.DELIVERY_CONFIRMATION:
    console.info('Delivery confirmation: ', incoming_message.content);
    return when.resolve(null);
  case IncomingMessageType.AUTHENTICATION:
    console.info("AUTH");
    console.log(JSON.stringify(incoming_message));
    var user_msg_id = incoming_message.sender;
    var splitted_content = incoming_message.content.split('---');
    return graph_api.queryGraphApi(splitted_content[0], categories)
      .then(function(info){
        return api.saveUserP(splitted_content[1], user_msg_id, splitted_content[0], info)
          .then(function(user) {
            console.log("User fb data: " + user);
          })
          .then(function() {
            return api.getUser(user_msg_id).then(function(user) {
              return scenarios.rankedScenarios(user).then(function(scens) {
                return api.updateRankedScenarios(user.user_msg_id, scens);
              });
            });
          })
          .catch(function (err) {
            console.warn(err);
            return when.resolve(null);
          });
      })
      .then(function() {
        return api.getUser(user_msg_id).then(function(user) {
          return initializeConversation(user, incoming_message);
        });
      })
      .catch(function(error) {
        console.warn(error);
        return when.resolve(null);
      });
  default:
    return when.reject(new Error('Unhandled message type: ' + incoming_message.type));
  }
};

function initializeConversation(user, incoming_message) {
  var user_name = user.user_info.name.split(' ');
  var state = {};
  state.scenario = scenarios.chooseNextScenario(user);
  state.step = 0;
  var hello_message = new TextResponse(
    incoming_message.sender, 'Hello ' + user_name[0] + '!');
  return api.updateState(incoming_message.sender, JSON.stringify(state))
    .then(function(updated) {
      var responses = [hello_message];
      return when.resolve(scenarios.scenarios[state.scenario].steps[state.step].getMessages(user, incoming_message)).then(function(scen_mess){
        return responses.concat(scen_mess);
      });
    });
}

module.exports = Bot;

