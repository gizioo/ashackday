var R = require('ramda');
var when = require('when');
var request = require('request');
var config = require('../config');

module.exports = {
  getLocationForCoords: getLocationForCoords
};

/**
 * @param {(string|number)} lat
 * @param {(string|number)} long
 * @returns {Promise} object with `city` and `country` properties
 */
function getLocationForCoords(lat, long) {
  var promise = when.promise(function(resolve, reject, notify) {
    request({
      url: 'https://maps.googleapis.com/maps/api/geocode/json',
      // api docs: https://developers.google.com/maps/documentation/geocoding/intro#ReverseGeocoding
      qs: {
        key: config.google_api_key,
        latlng: '' + lat + ',' + long,
        result_type: 'country|locality',
        language: 'en'
      },
      method: 'GET'
    }, function(error, response, body) {
      if (error) {
        return reject(error);
      }
      var resp = JSON.parse(body);
      if (resp.status !== 'OK') {
        return reject(new Error('Location query failed', resp.status, resp.error_message));
      }
      var city, country;
      var address_components = R.path(['results', 0, 'address_components'], resp);
      if (address_components) {
        address_components.forEach(function(address) {
          if (!country && address.types.indexOf('country') >= 0) {
            country = address.long_name;
          } else if (!city && address.types.indexOf('locality') >= 0) {
            city = address.long_name;
          }
        });
      }
      return resolve({city: city, country: country});
    });
  });
  return promise;
}
