/**
 * @readonly
 * @enum {number}
 */
var IncomingMessageType = {
  TEXT: 1,
  POSTBACK: 2,
  LOCATION: 3,
  AUTHENTICATION: 4,
  DELIVERY_CONFIRMATION: 300
};

module.exports = IncomingMessageType;