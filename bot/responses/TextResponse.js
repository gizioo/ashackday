var util = require('util');
var AbstractResponse = require('./AbstractResponse');
var ResponseType = require('./ResponseType');

/**
 * @constructor
 * @param {string} recipient
 * @param {string} text
 */
var TextResponse = function(recipient, text) {
  AbstractResponse.call(this, ResponseType.TEXT, recipient);
  this.text = text;
};
util.inherits(TextResponse, AbstractResponse);

TextResponse.prototype.getMessageData = function() {
  return {
    recipient: {
      id: this.recipient
    },
    message: {
      text: this.text
    }
  };
};

module.exports = TextResponse;
