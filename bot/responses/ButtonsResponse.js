var util = require('util');
var AbstractResponse = require('./AbstractResponse');
var ResponseType = require('./ResponseType');

/**
 * @typedef {Object} ButtonDef
 * @property {string} type `web_url` or `postback`
 * @property {string} title
 * @property {string} [url] either url or payload is required
 * @property {string} [payload]
 */

/**
 * @constructor
 * @param {string} recipient
 * @param {string} text
 * @param {ButtonDef[]} buttons
 */
var ButtonsResponse = function(recipient, text, buttons) {
  AbstractResponse.call(this, ResponseType.BUTTONS, recipient);
  this.text = text;
  this.buttons = buttons;
};
util.inherits(ButtonsResponse, AbstractResponse);

ButtonsResponse.prototype.getMessageData = function() {
  return {
    recipient: {
      id: this.recipient
    },
    message: {
      attachment: {
        type: 'template',
        payload: {
          template_type: 'button',
          text: this.text,
          buttons: this.buttons
        }
      }
    }
  };
};

module.exports = ButtonsResponse;
