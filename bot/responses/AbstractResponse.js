/**
 * Response base class
 * @constructor
 * @abstract
 * @param {ResponseType} type
 * @param {string} recipient id
 */
var AbstractResponse = function(type, recipient) {
  this.type = type;
  this.recipient = recipient;
};

AbstractResponse.prototype.getMessageData = function() {
  throw new Error('Cannot call abstract method');
};

module.exports = AbstractResponse;
