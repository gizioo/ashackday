/**
 * @readonly
 * @enum {number}
 */
var ResponseType = {
  TEXT: 1,
  BUTTONS: 2,
  GENERIC_TEMPLATE: 3
};

module.exports = ResponseType;
