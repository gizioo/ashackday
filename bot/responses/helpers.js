module.exports = {
  makeUrlButton: makeUrlButton,
  makePostbackButton: makePostbackButton,
  makeGenericTemplateElement: makeGenericTemplateElement
};

/**
 * @param {string} title
 * @param {string} url
 * @returns {ButtonDef}
 */
function makeUrlButton(title, url) {
  return {
    type: 'web_url',
    title: title,
    url: url
  };
}

/**
 * @param {string} title
 * @param {string} payload
 * @returns {ButtonDef}
 */
function makePostbackButton(title, payload) {
  return {
    type: 'postback',
    title: title,
    payload: payload
  };
}

/**
 * @param {string} title
 * @param {string} image_url
 * @param {string} subtitle
 * @param {ButtonDef[]} buttons
 * @returns {ElementDef}
 */
function makeGenericTemplateElement(title, image_url, subtitle, buttons) {
  return {
    title: title,
    image_url: image_url,
    subtitle: subtitle,
    buttons: buttons
  };
}
