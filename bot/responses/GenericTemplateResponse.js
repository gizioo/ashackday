var util = require('util');
var AbstractResponse = require('./AbstractResponse');
var ResponseType = require('./ResponseType');

/**
 * @typedef {Object} ElementDef
 * @property {string} title
 * @property {string} image_url
 * @property {string} subtitle
 * @property {ButtonDef[]} buttons
 */

/**
 * @constructor
 * @param {string} recipient
 * @param {ElementDef[]} elements
 */
var GenericTemplateResponse = function(recipient, elements) {
  AbstractResponse.call(this, ResponseType.GENERIC_TEMPLATE, recipient);
  this.elements = elements;
};
util.inherits(GenericTemplateResponse, AbstractResponse);

GenericTemplateResponse.prototype.getMessageData = function() {
  return {
    recipient: {
      id: this.recipient
    },
    message: {
      attachment: {
        type: 'template',
        payload: {
          template_type: 'generic',
          elements: this.elements
        }
      }
    }
  };
};

module.exports = GenericTemplateResponse;
