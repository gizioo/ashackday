/**
 * @constructor
 * @param {IncomingMessageType} type
 * @param {string} sender
 * @param {*} content
 */
var IncomingMessage = function(type, sender, content) {
  this.type = type;
  this.sender = sender;
  this.content = content;
};

module.exports = IncomingMessage;
