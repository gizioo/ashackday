var when = require('when');
var request = require('request');

var search_actions = ['search','seek','look','find','want','need', 'wan'];
var main_verbs = ['VBG','VBP','VB'];
var search_for = ['book','category','title', 'author'];
var specific_types = ['biography','guide','interview'];

var getLemmaForm = function(tokens, idx){
        var promise = when.promise(function(resolve, reject, notify) {
        tokens.forEach(function(tok){
                if (tok["$"].id == idx){
                        return resolve(tok.lemma);
                }
        });
        });
        return promise;
}
var getNLP = function(message){
    var that = this;
    var promise = when.promise(function(resolve, reject, notify) {
         request.post({
                url: 'http://hackdaybot.pl/learn/',
                method: 'POST',
                json: {
                        sentence: message
                }}, function(err, result, body){
                        return resolve(body.result);
                });
    }).then(parseAnswer);
    return promise;
}

var parseAnswer = function(result){

    var promise = when.promise(function(resolve, reject, notify) {
        var action = "unknown";
        var subject = '';
        var type = '';
	var specific = '';
		var named_entities = {};
        var tokens =  result.document.sentences.sentence.tokens.token;
	if(Array.isArray(tokens)){
          tokens.forEach(function(token){
             if(search_actions.indexOf(token.lemma) >= 0 && main_verbs.indexOf(token.POS) >= 0){
                action = "search";
                return;
            }else if(token.lemma.toLowerCase().indexOf("fuck")>=0){
                        action = "rude";
                        return;
            }else if(token.NER !== "O"){
		if(named_entities[token.NER]){
			named_entities[token.NER] = named_entities[token.NER] + " " + token.lemma;
		}else{
			named_entities[token.NER] = token.lemma;
		}
	   }
        });
	}else{
	    if(search_actions.indexOf(tokens.lemma) >= 0 && main_verbs.indexOf(tokens.POS) >= 0){
                action = "You want to search!";
                return;
            }else if(tokens.lemma.toLowerCase().indexOf("fuck")>=0){
                        action = "You are being rude!";
                        return;
            }
	}
	
	if(Array.isArray(result.document.sentences.sentence.dependencies[0].dep)){
            when.resolve(
                result.document.sentences.sentence.dependencies[1].dep.forEach(function(dependancy){
                    if(dependancy["$"].type === "nmod:for" || dependancy["$"].type === "ccomp"){
                        when.resolve(getLemmaForm(tokens, dependancy.dependent["$"].idx)).then(function(result){
                        if(search_for.indexOf(result) >= 0){
                                subject = result;
                        }else if(specific_types.indexOf(result)>=0){
				specific = result;
			}
                        });
                    }else if(dependancy["$"].type === "nmod:about"){
						when.resolve(getLemmaForm(tokens, dependancy.governor["$"].idx)).then(function(result){
							subject = result;
							type =  dependancy.dependent["_"];
						});
		   }else if(dependancy["$"].type === "dobj"){
			when.resolve(getLemmaForm(tokens, dependancy.dependent["$"].idx)).then(function(result){
				if(search_for.indexOf(result) >= 0){
                                	subject = result;
	                        }else if(specific_types.indexOf(result)>=0){
        	                        specific = result;
                	        }
			});
		   }else if(dependancy["$"].type === "compound"){
		        when.resolve(getLemmaForm(tokens, dependancy.dependent["$"].idx)).then(function(result){
				if(type){
					type+=" ";
				}
		        	type = result;
		        });
		   }else if(dependancy["$"].type === "amod"){
			if(type){
				type+=" ";
			}
			type += dependancy.governor["_"] + " " + dependancy.dependent["_"];
		   }
               		
                })).then(function(){
            		if(!(subject || type)){
            			when.resolve(result.document.sentences.sentence.dependencies[0].dep.forEach(function(dependancy){
		                    if(dependancy["$"].type === "nmod" || dependancy["$"].type === "dobj"){
		                        when.resolve(getLemmaForm(tokens, dependancy.dependent["$"].idx)).then(function(result){
		                        if(search_for.indexOf(result) >= 0){
		                                subject = result;
					}else if(specific_types.indexOf(result)>=0){
                                                specific = result;
                                        }
		                        });
		                    }else if(dependancy["$"].type === "compound"){
		                        when.resolve(getLemmaForm(tokens, dependancy.dependent["$"].idx)).then(function(result){
		                                type = result;
		                        });
		                    }
               			 })).then(function(){
            				return resolve({action:action, subject: subject, type: type, named_entities: named_entities, specific: specific});	
            			 });
            		}else{
            			return resolve({action:action, subject: subject, type: type, named_entities: named_entities, specific: specific});	
            		}
                   
               });
	   }else{
 	           return resolve({action:action, subject: subject, type: result.document.sentences.sentence.dependencies[0].dep.dependent["_"], named_entities: named_entities});
	   }
    });
    return promise;
}

module.exports = {
  getNLP: getNLP
}
