## Scraper

Create `database` and set correct values at the beginning of scraper.py:

```
DB_NAME = ''
DB_USER = ''
DB_PASSWORD = ''
DB_HOST = ''
```

then paste sql into PostgreSQL
```
CREATE TABLE books
(
  book_id uuid NOT NULL DEFAULT uuid_generate_v4(),
  book_title text,
  book_author text,
  book_genre text,
  book_img_url text,
  book_url text
)
```

then install virtual-env and requirements
```
virtualenv scraper
source scraper/bin/activate
pip install -r requirements.txt
```

then run the scraper
```
python scraper.py
```


