import requests
import bs4
import psycopg2
import re

DB_NAME = 'hackday_bot'
DB_USER = 'postgres'
DB_PASSWORD = 'test123'
DB_HOST = 'localhost'

try:
    conn = psycopg2.connect('dbname='+DB_NAME+' user='+DB_USER+' host='+DB_HOST+' password='+DB_PASSWORD)
except:
    print "Connection error..."

cur = conn.cursor()

genres_addr = 'https://www.goodreads.com/genres/list/'
genres_addr_p2 = 'https://www.goodreads.com/genres/list?page=2'
genres_addr_p3 = 'https://www.goodreads.com/genres/list?page=3'

format = 'lxml'
pattern = 'a.actionLinkLite'

genres = bs4.BeautifulSoup(requests.get(genres_addr).text, format).select(pattern) + \
         bs4.BeautifulSoup(requests.get(genres_addr_p2).text, format).select(pattern) + \
         bs4.BeautifulSoup(requests.get(genres_addr_p3).text, format).select(pattern)

genres_t = []

for item in genres:
    if item.get_text().encode('utf-8').strip() not in ['adolescence','amazon','art-books-monographs',
                                                       'bolivia', 'cookbooks', 'cultural-studies',
                                                       'emergency-services', 'figure-skating', 'fire-services',
                                                       'game-design','history-civil-war-eastern-theater','huguenots',
                                                       'hydrogeology','intensive-care','light-novel','luxemburg',
                                                       'manhwa','numismatics','pediatricians','pediatrics',
                                                       'railway-history','railways','suisse','tasmania','vespa']:
        genres_t.append(item.get_text().lower().replace(' ', '-'))

for genre in genres_t:
    print genre
    data_response = requests.get('https://www.goodreads.com/genres/' + genre)
    soup = bs4.BeautifulSoup(data_response.text, format)

    data = soup.select('div.leftAlignedImage')

    j = 0
    for item in data:
        if item is not None:
            scripts = item.find_all('script')
            if j == 0:
                first_occur = str(scripts[1]).replace("\\", "")
            else:
                first_occur = str(scripts[0]).replace("\\", "")
            if(first_occur is not None):
                author = re.search(r'.*class="authorName".*>([a-zA-Z\s]+)\<\/a>', first_occur)
                if(author is not None):
                    cur.execute("INSERT INTO books (book_title, book_author, book_genre, book_img_url, book_url) " \
                            "VALUES (%s, %s, %s, %s, %s)",
                            (item.img['alt'],
                             author.group(1),
                             genre, item.img['src'],
                             'https://www.goodreads.com' + item.a['href']))
            j += 1
            conn.commit()

cur.close()
conn.close()
print 'Scraping finished.'
